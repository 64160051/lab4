import java.util.Scanner;

public class Array12 {
    public static void main(String[] args) {
        printWelcome();
        while(true){
            printMenu();
            int choice = inputChoice();
            switch(choice){
                case 1:
                    printHelloWorldNTime();
                    break;
                case 2:
                    addTwoNumber();
                    break;
                case 3:
                    exitProgram();
                break;
            }
        }
    }

    private static void exitProgram() {
        System.out.println("Bye!!!");
        System.exit(0);
        
    }

    private static void addTwoNumber() {
        Scanner sc = new Scanner(System.in);
        int first, second;
        int result;
        System.out.print("Please input first number: ");
        first = sc.nextInt();
        System.out.print("Please input second number: ");
        second = sc.nextInt();
        result = first + second;
        System.out.println("Result = "+ result);
    }

    private static void printHelloWorldNTime() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        for(int i=0; i<time; i++){
            System.out.println("Hello World!!!");
        }
        
    }

    private static int inputChoice() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input your choice(1-3): ");
            int choice = sc.nextInt();
            if(choice>=1 && choice<=3){
                return choice;
            }else{
                System.out.println("Error: Please input number between 1-3!!!");
            }
        }
    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    private static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }
}
